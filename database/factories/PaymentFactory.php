<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Payment;
use Faker\Generator as Faker;

$factory->define(Payment::class, function (Faker $faker) {
    return [
        'sum' => $faker->numberBetween(1, 1000),
        'account_id' => function () {
            return factory(App\Account::class)->create()->id;
        }
    ];
});
