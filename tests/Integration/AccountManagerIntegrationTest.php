<?php

namespace Tests\Integration;

use Tests\TestCase;
use App\Account;
use App\Payment;
use App\Services\AccountManager;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;

class AccountManagerIntegrationTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    protected $am;
    protected $oldPayment;
    protected $otherAccount;

    public function setUp(): void
    {
        parent::setUp();
        $this->am = new AccountManager();
        $this->oldPayment = factory(Payment::class)->create();
        $this->otherAccount = factory(Account::class)->create();
    }

    /**
     * with switch account (was changed account_id in new Payment)
     */
    public function testUpdateAccountsWithSwitchAccountInPayments()
    {
        $newPayment = [
            'sum' => $this->faker->numberBetween(1, 1000),
            'account_id' => $this->otherAccount->id
        ];

        $oldBalanceOldAccount = (int)$this->oldPayment->account->balance;
        $oldBalanceOtherAccount = (int)$this->otherAccount->balance;

        $this->am->updateAccounts($newPayment, $this->oldPayment);

        $this->assertDatabaseHas('accounts', [
            'balance' => $oldBalanceOldAccount - $this->oldPayment->sum,
            'id' => $this->oldPayment->account_id
        ]);

        $this->assertDatabaseHas('accounts', [
            'balance' => $oldBalanceOtherAccount + $newPayment['sum'],
            'id' => $this->otherAccount->id
        ]);
    }
}