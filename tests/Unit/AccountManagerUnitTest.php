<?php

namespace Tests\Unit;

use App\Services\AccountManager;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Payment;
use App\Account;

class AccountManagerUnitTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    protected $am;

    public function setUp(): void
    {
        parent::setUp();
        $this->am = new AccountManager();
    }

    /**
     * without switch account (was not changed account_id in new Payment)
     */
    public function testCalculateNewBalanceWithoutSwitchAccount()
    {
        $oldPayment = factory(Payment::class)->make();
        $newPayment = [
            'sum' => $this->faker->numberBetween(1, 1000),
            'account_id' => $oldPayment->account_id
        ];
        $oldBalance = (int)$oldPayment->account->balance;


        $responseAccount = $this->am->calculateNewBalance($newPayment, $oldPayment, $oldPayment->account);

        $needleBalance = $oldBalance - ($oldPayment->sum - $newPayment['sum']);

        $this->assertEquals($responseAccount->balance, $needleBalance);
    }

    /**
     * with switch account (was changed account_id in new Payment)
     */
    public function testCalculateNewBalanceWithSwitchAccount()
    {
        $oldPayment = factory(Payment::class)->make();
        $otherAccount = factory(Account::class)->make();
        $newPayment = [
            'sum' => $this->faker->numberBetween(1, 1000),
            'account_id' => $otherAccount->id
        ];
        $oldBalanceOldAccount = (int)$oldPayment->account->balance;
        $oldBalanceOtherAccount = (int)$otherAccount->balance;

        $responseOldAccount = $this->am->calculateNewBalance($newPayment, $oldPayment, $oldPayment->account);
        $responseOtherAccount = $this->am->calculateNewBalance($newPayment, $oldPayment, $otherAccount);

        $this->assertEquals($responseOldAccount->balance, $oldBalanceOldAccount - $oldPayment->sum);
        $this->assertEquals($responseOtherAccount->balance, $oldBalanceOtherAccount + $newPayment['sum']);
    }
}
