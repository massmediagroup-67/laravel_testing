<?php

namespace Tests\Browser;

use App\Account;
use App\Payment;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;

class PaymentBrowserTest extends DuskTestCase
{
    use WithFaker;

    protected $ac;
    protected $sum;
    protected $payment;

    public function setUp(): void
    {
        parent::setUp();
        $this->ac = Account::first();
        $this->payment = Payment::first();
        $this->sum = $this->faker->numberBetween(1, 1000);
    }

    public function testPaymentCreate()
    {
        $this->browse(function (Browser $browser) {

            $browser->visit('http://localhost:8000/payment/create')
                ->type('sum', $this->sum)
                ->select('account_id', $this->ac->id)
                ->click('@create-button');
        });

        $this->assertDatabaseHas('payments', [
            'sum' => $this->sum,
            'account_id' => $this->ac->id,
        ]);
    }

    public function testPaymentUpdate()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('http://localhost:8000/payment/'.$this->payment->id.'/edit')
                ->type('sum', $this->sum)
                ->select('account_id', $this->ac->id)
                ->click('@update-button');
        });

        $this->assertDatabaseHas('payments', [
            'sum' => $this->sum,
            'account_id' => $this->ac->id,
        ]);
    }

    public function testPaymentDelete()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('http://localhost:8000/payment/'.$this->payment->id)
                ->click('@delete-button');
        });

        $this->assertDatabaseMissing('payments', [
            'id' => $this->payment->id,
            'sum' => $this->payment->sum,
            'account_id' => $this->payment->account_id,
        ]);
    }
}
