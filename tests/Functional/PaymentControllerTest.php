<?php

namespace Tests\Functional;

use App\Payment;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PaymentControllerTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    protected $payment;

    public function setUp(): void
    {
        parent::setUp();
        $this->payment = factory(Payment::class)->create();
    }

    public function testPaymentCreate()
    {
        $this->post('/payment/create', [
            'sum' => $this->payment->sum,
            'account_id' => $this->payment->account->id
        ]);

        $this->assertDatabaseHas('payments', [
            'sum' => $this->payment->sum,
            'account_id' => $this->payment->account->id,
        ]);
    }

    public function testPaymentUpdate()
    {
        $oldPayment = factory(Payment::class)->create();
        $this->put("/payment/$oldPayment->id", [
            'sum' => $this->payment->sum,
            'account_id' => $this->payment->account->id
        ]);

        $this->assertDatabaseHas('payments', [
            'sum' => $this->payment->sum,
            'account_id' => $this->payment->account->id,
        ]);
    }

    public function testPaymentDelete()
    {
        $id = $this->payment->id;
        $this->delete("/payment/$id");

        $this->assertDatabaseMissing('payments', [
            'id' => $this->payment->id,
            'sum' => $this->payment->sum,
            'account_id' => $this->payment->account->id,
        ]);
    }
}
