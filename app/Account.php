<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Account extends Model
{
    public $timestamps = false;
    public $fillable = ['balance'];

    public function payments()
    {
        return $this->hasMany('App\Payment');
    }

    public function addSum($sum)
    {
        $this->balance += $sum;
        return $this;
    }

    public function deleteSum($sum)
    {
        $this->balance -= $sum;
        return $this;
    }
}
