<?php

namespace App\Http\Controllers;

use App\Account;
use App\Http\Requests\PaymentRequest;
use App\Payment;
use App\Services\AccountManager;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('payment.index', ['payments' => Payment::all()]);
    }

    /**
     * Show the Payment by id.
     *
     * @param Payment $payment
     * @return Response
     */
    public function show(Payment $payment)
    {
        return view('payment.show', ['payment' => $payment]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('payment.create', ['accounts' => Account::All()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return void
     */
    public function store(PaymentRequest $request)
    {
        if ($request->validated()) {
            $payment = Payment::create($request->input());
            $payment->account->addSum($payment->sum)->save();
        }

        return redirect()->route('payment.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Payment $payment
     * @return Response
     */
    public function edit(Payment $payment)
    {
        return view('payment.edit', ['accounts' => Account::All(), 'payment' => $payment]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param AccountManager $am
     * @param Payment $payment
     * @return void
     */
    public function update(PaymentRequest $request, AccountManager $am, Payment $payment)
    {
        if ($request->validated()) {
            $am->updateAccounts($request->input(), $payment);
            $payment->update($request->input());
        }

        return redirect()->route('payment.index');
    }

    /**
     * Remove the specified resource from storage.
     * @param Payment $payment
     * @return void
     * @throws \Exception
     */
    public function destroy(Payment $payment)
    {
        $payment->account->deleteSum($payment->sum)->save();
        $payment->delete();

        return redirect()->route('payment.index');
    }
}
