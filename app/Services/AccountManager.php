<?php


namespace App\Services;

use App\Payment;
use App\Account;

class AccountManager
{
    public function calculateNewBalance(array $newPayment, Payment $oldPayment, $account)
    {
        if ($oldPayment->account_id == $newPayment['account_id']) {
            $account->balance -= $oldPayment->sum - $newPayment['sum'];
        } elseif ($newPayment['account_id'] == $account->id) {
            $account->balance += $newPayment['sum'];
        } elseif ($oldPayment->account_id == $account->id) {
            $account->balance -= $oldPayment->sum;
        }

        return $account;
    }

    public function updateAccounts(array $newPayment, Payment $payment)
    {
        $this->calculateNewBalance($newPayment, $payment, $payment->account)->save();

        if ($payment->account_id != $newPayment['account_id']) {
            $newAccount = Account::find($newPayment['account_id']);
            $this->calculateNewBalance($newPayment, $payment, $newAccount)->save();
        }
    }
}
