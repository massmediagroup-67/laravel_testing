@extends('base')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-2"></div>
            <div class="col-8">
                <form method="POST" action="{{ action('PaymentController@update',['id' => $payment->id]) }}" class="w-100">
                    {{ csrf_field() }}
                    @method('PUT')
                    @component('payment.form-group', ['label' => 'Sum:', 'name' => 'sum'])
                        <input type="text" class="form-control @error('sum') is-invalid @enderror" id="sum" name="sum" value="{{ $payment->sum }}">
                    @endcomponent
                    @component('payment.form-group', ['label' => 'Account:', 'name' => 'account_id'])
                        <select  id="account_id" name="account_id" class="form-control @error('account_id') is-invalid @enderror">
                            <option disabled>Select account</option>
                            @foreach($accounts as $account)
                                <option @if($payment->account_id === $account->id) selected @endif value="{{ $account->id }}"> {{ $account->id }}</option>
                            @endforeach
                        </select>
                    @endcomponent
                    <button type="submit" class="btn btn-primary" dusk="update-button">Edit</button>
                </form>
            </div>
            <div class="col-2"></div>
        </div>
    </div>
@endsection