@extends('base')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-2"></div>
            <div class="col-8">
                <form method="POST" action="{{ action('PaymentController@store') }}" class="w-100">
                    {{ csrf_field() }}
                    @component('payment.form-group', ['label' => 'Sum:', 'name' => 'sum'])
                        <input type="text" class="form-control @error('sum') is-invalid @enderror" id="sum" name="sum">
                    @endcomponent
                    @component('payment.form-group', ['label' => 'Account:', 'name' => 'account_id'])
                        <select  id="account_id" name="account_id" class="form-control @error('account_id') is-invalid @enderror">
                            <option disabled>Select account</option>
                            @foreach($accounts as $account)
                                <option value="{{ $account->id }}"> {{ $account->id }}</option>
                            @endforeach
                        </select>
                    @endcomponent
                    <button type="submit" class="btn btn-primary" dusk="create-button">Create</button>
                </form>
            </div>
            <div class="col-2"></div>
        </div>
    </div>
@endsection