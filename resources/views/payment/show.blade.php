@extends('base')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-2"></div>
            <div class="col-8">
                <div class="mb-3">
                    {{ $payment->sum }} <br>
                    <a href=" {{ route('payment.edit',['id' => $payment->id]) }} ">Edit</a>
                    <form method="POST" action="{{ action('PaymentController@destroy',['id' => $payment->id]) }}"
                          class="w-100">
                        {{ csrf_field() }}
                        @method('DELETE')
                        <button type="submit" class="btn btn-primary" dusk="delete-button">Delete</button>
                    </form>
                </div>
            </div>
            <div class="col-2"></div>
        </div>
    </div>
@endsection
