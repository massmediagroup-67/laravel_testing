@extends('base')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-2"></div>
            <div class="col-8">
                @foreach ($payments as $payment)
                    <div class="mb-3">
                            <a href=" {{ route('payment.show',['id' => $payment->id]) }} ">{{ $payment->id }}</a>
                    </div>
                @endforeach
            </div>
            <div class="col-2"></div>
        </div>
    </div>
@endsection