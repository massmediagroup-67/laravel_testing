<div class="form-group">
    <label for="title">{{ $label }}</label>
    {{ $slot }}
    @error($name)
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
</div>